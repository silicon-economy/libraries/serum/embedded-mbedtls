# Changelog

## Current Development

## v0.2.0
- Replace `cty` dependency by `core::ffi` for C types
- Generate config.h in build.rs
  - Add `MBEDTLS_SSL_SRV_C` and `MBEDTLS_SSL_CLI_C` features

## v0.1.0
 - MbedTLS 3.6.0
 - Hardcoded mbedtls config
  - TLS/DTLS 1.2
  - CCM PSK (`TLS_PSK_WITH_AES_128_CCM_8`)
  - CTR_DRBG pseudo-random number generator

