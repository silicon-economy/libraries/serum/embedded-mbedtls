// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! This tool forwards data between a serial port and an actual UDP socket. It can be used to run
//! examples from development boards without actual IP hardware.

use base64::engine::Engine;
use std::collections::VecDeque;
use std::io::{Read, Write};
use std::net::{Ipv4Addr, UdpSocket};
use try_ascii::TryAscii;

fn main() {
    let serial_port_path = std::env::args().nth(1).expect("No serial port name given");
    let mut serial = serialport::new(serial_port_path, 9600)
        .open()
        .expect("could not open '{serial_port_path}'");

    let udp =
        UdpSocket::bind((Ipv4Addr::UNSPECIFIED, 0)).expect("Could not create/bind UDP socket");
    udp.connect(
        std::env::args()
            .nth(2)
            .expect("No DTLS server address+port given"),
    )
    .expect("Could not connect UDP socket");
    udp.set_nonblocking(true)
        .expect("Could not configure UDP socket as nonblocking");

    println!("Serial: {serial:?}");
    println!("Socket: {}", udp.local_addr().unwrap());

    let mut serial_buffer = VecDeque::new();

    loop {
        let mut buf = [0u8; 2000];

        // Read/Forward serial
        let num_bytes = serial
            .bytes_to_read()
            .expect("Could not determine the number of available serial bytes")
            as usize;
        if num_bytes > 0 {
            serial
                .read(&mut buf[..num_bytes])
                .expect("Could not read serial bytes");
            serial_buffer.extend(&buf[..num_bytes]);

            // Forward to UDP if newline found
            if let Some(newline_pos) = serial_buffer.iter().position(|byte| byte == &b'\n') {
                let mut remaining = serial_buffer.split_off(newline_pos);
                remaining.pop_front();
                let mut line = serial_buffer;
                serial_buffer = remaining;
                println!("Decoding: '{:?}'", line.make_contiguous().try_ascii());
                println!("Decoding: '{:?}'", line.make_contiguous());
                // After flashing, we sometimes receive some garbage first so we gracefully handle
                // these errors here. For the communication, this should just feel like a lost UDP
                // packet so it should be able to cope with this.
                match base64::prelude::BASE64_STANDARD.decode(line.make_contiguous()) {
                    Ok(bytes) => {
                        println!(
                            "Serial->UDP: {} bytes ({} bytes base64)",
                            bytes.len(),
                            line.len()
                        );
                        udp.send(&bytes).expect("Could not send UDP packet");
                    }
                    Err(e) => {
                        println!("Could not decode base64 serial input bytes: {e:?}");
                    }
                }
            }
        }

        // Read/Forward UDP
        match udp.recv(&mut buf) {
            Ok(len) => {
                let serial_bytes = base64::prelude::BASE64_STANDARD.encode(&buf[..len]);
                println!(
                    "UDP->Serial: {} bytes ({} bytes base64)",
                    len,
                    serial_bytes.len(),
                );
                serial
                    .write(serial_bytes.as_bytes())
                    .expect("Could not write serial bytes");
                serial.write(&[b'\n']).expect("Could not write serial \\n");
            }
            Err(e) if e.kind() == std::io::ErrorKind::WouldBlock => {}
            Err(e) => {
                panic!("{e:?}");
            }
        }
    }
}
