// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! This BSP implements the required hardware interface for the STM32 NUCLEO-L452RE development
//! board
//!
//! The UDP data is sent/read as base64-encoded data to/from the serial port which allows to run
//! this example without networking hardware on the very cheap nucleo-l452re development board. To
//! translate/forward this data, the `util-forward-serial-udp` utility can be used. For example,
//! it can be used to forward to a DTLS (test) server running locally on port 12345 like this:
//! `cargo run -- <serial-device> localhost:12345`

//#![deny(warnings)]
#![no_main]
#![no_std]
#![feature(alloc_error_handler)] // for the allocator module
#![feature(c_size_t)] // for using core::ffi::c_size_t in calloc

#[cfg(not(any(feature = "dtls-psk-client", feature = "dtls-psk-client-sys")))]
compile_error!("Select one of the available examples by activating the corresponding feature, e.g. 'dtls-psk-client'");

mod allocator;
mod calloc;

use hal::dma::CircReadDma;
use hal::prelude::*;
use hal::serial::Serial;
use stm32l4xx_hal as hal;

use embedded_mbedtls_sys as tls;

use cortex_m::asm;
use cortex_m_rt::{entry, exception};

use panic_rtt_target as _;

use core::ffi;
use core::net;
use core::sync::atomic::{AtomicU32, Ordering};

use base64::engine::Engine;

static CURRENT_MILLIS: AtomicU32 = AtomicU32::new(0);

#[allow(unused)]
fn exit(ret: ffi::c_int) -> ! {
    log::error!("Last error was: -0x{:x}", -ret);
    asm::bkpt();
    loop {
        continue;
    }
}

#[allow(unused)]
unsafe extern "C" fn my_entropy_fn(
    entropy_context: *mut ffi::c_void,
    buf: *mut ffi::c_uchar,
    buf_len: usize,
) -> ffi::c_int {
    let rng: &mut hal::rng::Rng = &mut *(entropy_context as *mut hal::rng::Rng);
    let bytes = core::slice::from_raw_parts_mut(buf, buf_len);
    use hal::rng::RngCore;
    rng.fill_bytes(bytes);
    0
}

#[allow(unused)]
#[derive(Default)]
struct MyTimer {
    start: u32,
    int_ms: u32,
    fin_ms: u32,
}

#[allow(unused)]
unsafe extern "C" fn my_set_timer_fn(timer: *mut ffi::c_void, int_ms: u32, fin_ms: u32) {
    let timer: &mut MyTimer = &mut *(timer as *mut MyTimer);
    timer.start = CURRENT_MILLIS.load(Ordering::Relaxed);
    timer.int_ms = int_ms;
    timer.fin_ms = fin_ms;
}

#[allow(unused)]
unsafe extern "C" fn my_get_timer_fn(timer: *mut ffi::c_void) -> ffi::c_int {
    let timer: &mut MyTimer = &mut *(timer as *mut MyTimer);
    let duration = CURRENT_MILLIS.load(Ordering::Relaxed) - timer.start;
    if timer.fin_ms == 0 {
        -1
    } else if duration >= timer.fin_ms {
        2
    } else if duration >= timer.int_ms {
        1
    } else {
        0
    }
}

struct MyIo {
    tx: hal::serial::Tx<hal::device::USART2>,
    rx: CircBufferReader,
    serial_rx_buffer: heapless::Vec<u8, 2000>,
}

impl MyIo {
    fn my_send(&mut self, data: &[u8]) {
        // base64 encoding
        let mut base64_buf = heapless::Vec::<u8, 2000>::new();
        base64_buf
            .resize_default(2000)
            .expect("Buffer for decoded message should be long enough");
        let len = base64::prelude::BASE64_STANDARD
            .encode_slice(&data, base64_buf.as_mut())
            .expect("Could not decode base64 serial input bytes");
        base64_buf.truncate(len);
        // Transmit all (base64-encoded) bytes with trailing \n
        for b in base64_buf {
            nb::block!(self.tx.write(b)).expect("Could not transmit serial byte");
        }
        nb::block!(self.tx.write(b'\n')).expect("Could not transmit serial byte");
    }

    fn my_recv(&mut self, rx_buf: &mut [u8]) -> nb::Result<usize, hal::serial::Error> {
        // Read as many serial input bytes until \n is found or no byte available at all
        loop {
            let byte = self.rx.read()?;
            if byte == b'\n' {
                // Decode base64 into rx_buf and reset serial_rx_buffer
                let len = base64::prelude::BASE64_STANDARD
                    .decode_slice(&self.serial_rx_buffer, rx_buf)
                    .expect("Could not decode base64 serial input bytes");
                self.serial_rx_buffer.clear();
                return Ok(len);
            } else {
                self.serial_rx_buffer
                    .push(byte)
                    .expect("serial_rx_buffer too short");
            }
        }
    }
}

#[allow(unused)]
unsafe extern "C" fn my_send_fn(
    io_context: *mut ffi::c_void,
    buf: *const ffi::c_uchar,
    buf_len: usize,
) -> ffi::c_int {
    // Reconstruct Rust types from C types
    let io: &mut MyIo = &mut *(io_context as *mut MyIo);
    let data = core::slice::from_raw_parts(buf, buf_len);
    // Send or panic
    io.my_send(data);
    // Return number of transmitted bytes
    data.len() as i32
}

#[allow(unused)]
unsafe extern "C" fn my_recv_fn(
    io_context: *mut ffi::c_void,
    buf: *mut ffi::c_uchar,
    buf_len: usize,
) -> ffi::c_int {
    // Reconstruct Rust types from C types
    let io: &mut MyIo = &mut *(io_context as *mut MyIo);
    let rx_buf = core::slice::from_raw_parts_mut(buf, buf_len);
    // Try to receive a full datagram
    match io.my_recv(rx_buf) {
        // Translate Rust return types to mbedtls return/error codes
        Ok(len) => len as i32,
        Err(nb::Error::WouldBlock) => tls::MBEDTLS_ERR_SSL_WANT_READ,
        Err(nb::Error::Other(e)) => panic!("{e:?}"),
    }
}

/// Simple wrapper type to implement [`embedded_hal::serial::Read`] on a
/// [`stm32l4xx_hal::dma::CircBuffer`].
pub struct CircBufferReader(pub(crate) hal::dma::CircBuffer<[u8; 2048], hal::serial::RxDma2>);

impl embedded_hal::serial::Read<u8> for CircBufferReader {
    type Error = hal::serial::Error; // we translate from hal::dma::Error
    fn read(&mut self) -> nb::Result<u8, hal::serial::Error> {
        let mut buf = [0u8; 1];
        match self.0.read(&mut buf) {
            Ok(1) => Ok(buf[0]),
            Ok(0) => Err(nb::Error::WouldBlock),
            Ok(num) => panic!("{} bytes read into a 1 byte read buffer", num),
            Err(hal::dma::Error::Overrun) => Err(nb::Error::Other(hal::serial::Error::Overrun)),
            Err(_) => Err(nb::Error::Other(hal::serial::Error::Noise)),
        }
    }
}

#[derive(Debug)]
struct MilliSecondClock;

impl embedded_timers::clock::Clock for MilliSecondClock {
    type Instant = embedded_timers::instant::Instant64<1000>;
    fn now(&self) -> Self::Instant {
        let current_millis = CURRENT_MILLIS.load(Ordering::Relaxed);
        embedded_timers::instant::Instant64::new(current_millis as u64)
    }
}

/// The return type of UdpClientStack::recv needs to contain the SocketAddr where the datagram was
/// received from. Therefore, we store the remote from the UdpClientStack::connect call in the
/// socket and assume that all received data comes from that remote.
#[derive(Default)]
struct MySocket {
    socket_addr: Option<net::SocketAddr>,
}

// Simplified UdpClientStack implementation on the MyIo type
impl embedded_nal::UdpClientStack for MyIo {
    type UdpSocket = MySocket;
    type Error = ();

    fn socket(&mut self) -> Result<Self::UdpSocket, Self::Error> {
        Ok(MySocket::default())
    }

    fn connect(
        &mut self,
        socket: &mut Self::UdpSocket,
        remote: net::SocketAddr,
    ) -> Result<(), Self::Error> {
        socket.socket_addr = Some(remote);
        Ok(())
    }

    fn send(&mut self, _sock: &mut Self::UdpSocket, buffer: &[u8]) -> nb::Result<(), Self::Error> {
        self.my_send(buffer);
        Ok(())
    }

    fn receive(
        &mut self,
        socket: &mut Self::UdpSocket,
        buffer: &mut [u8],
    ) -> nb::Result<(usize, net::SocketAddr), Self::Error> {
        match self.my_recv(buffer) {
            Ok(len) => Ok((len, socket.socket_addr.unwrap())),
            Err(nb::Error::WouldBlock) => Err(nb::Error::WouldBlock),
            Err(nb::Error::Other(e)) => panic!("{e:?}"),
        }
    }

    fn close(&mut self, _socket: Self::UdpSocket) -> Result<(), Self::Error> {
        Ok(())
    }
}

#[entry]
fn main() -> ! {
    rtt_log::init();
    rtt_target::rprintln!("Hello, rprintln!");
    log::info!("Hello, log!");

    allocator::init_allocator();

    let dp = hal::stm32::Peripherals::take().unwrap();
    let cp = cortex_m::Peripherals::take().unwrap();

    let mut flash = dp.FLASH.constrain();
    let mut rcc = dp.RCC.constrain();
    let mut pwr = dp.PWR.constrain(&mut rcc.apb1r1);

    let mut gpioa = dp.GPIOA.split(&mut rcc.ahb2);

    let clocks = rcc
        .cfgr
        .sysclk(80.MHz())
        .pclk1(80.MHz())
        .pclk2(80.MHz())
        .hsi48(true)
        .freeze(&mut flash.acr, &mut pwr);

    // Init Systick to 1 tick per millisecond
    let mut syst = cp.SYST;
    syst.set_clock_source(cortex_m::peripheral::syst::SystClkSource::Core);
    syst.set_reload(80_000);
    syst.clear_current();
    syst.enable_counter();
    syst.enable_interrupt();

    let rng = dp.RNG.enable(&mut rcc.ahb2, clocks);

    let tx = gpioa
        .pa2
        .into_alternate(&mut gpioa.moder, &mut gpioa.otyper, &mut gpioa.afrl);
    let rx = gpioa
        .pa3
        .into_alternate(&mut gpioa.moder, &mut gpioa.otyper, &mut gpioa.afrl);

    let serial = Serial::usart2(dp.USART2, (tx, rx), 9_600.bps(), clocks, &mut rcc.apb1r1);
    let (tx, rx) = serial.split();
    let dma1_channels = dp.DMA1.split(&mut rcc.ahb1);
    let rxbuf = cortex_m::singleton!(: [u8; 2048] = [0; 2048]).unwrap();
    let rx_circ_buffer = rx.with_dma(dma1_channels.6).circ_read(rxbuf);
    let rx = CircBufferReader(rx_circ_buffer);

    let io = MyIo {
        tx,
        rx,
        serial_rx_buffer: heapless::Vec::new(),
    };

    #[cfg(feature = "dtls-psk-client-sys")]
    run_dtls_psk_client_sys(rng, io);

    #[cfg(feature = "dtls-psk-client")]
    run_dtls_psk_client(rng, io);

    // if all goes well you should reach this breakpoint
    asm::bkpt();
    loop {
        continue;
    }
}

#[allow(unused)]
fn run_dtls_psk_client_sys(mut rng: hal::rng::Rng, mut io: MyIo) {
    let my_entropy_ctx = &mut rng as *mut hal::rng::Rng as *mut ffi::c_void;

    let my_io_ctx = &mut io as *mut MyIo as *mut ffi::c_void;

    let mut timer = MyTimer::default();
    let my_timer_ctx = &mut timer as *mut MyTimer as *mut ffi::c_void;

    unsafe {
        dtls_psk_client_sys::run(
            my_entropy_fn,
            my_entropy_ctx,
            my_io_ctx,
            my_send_fn,
            my_recv_fn,
            my_timer_ctx,
            my_set_timer_fn,
            my_get_timer_fn,
            exit,
        )
    };
}

#[allow(unused)]
fn run_dtls_psk_client(mut rng: hal::rng::Rng, udp_stack: MyIo) {
    let clock = MilliSecondClock;
    dtls_psk_client::run(udp_stack, clock, &mut rng);
}

#[exception]
#[allow(non_snake_case)]
unsafe fn HardFault(ef: &cortex_m_rt::ExceptionFrame) -> ! {
    panic!("{:#?}", ef);
}

#[exception]
#[allow(non_snake_case)]
fn SysTick() {
    CURRENT_MILLIS.fetch_add(1, Ordering::Relaxed);
}
