// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! For using mbedtls in the application, the board needs to provide the C functions `calloc` and
//! `free`. We implement those two on top of the Rust `alloc` library.
//!
//! The function interface looks a little bit different: C's `free` only knows about the allocated
//! memory location but Rust's `dealloc` requires us to specify the `Layout` that was used for
//! allocation (i.e. the size + alignment). To translate between those two interfaces, we store
//! the `Layout` in the allocated memory region, i.e. we allocate `size_of::<Layout>()` more than
//! requested by the `calloc` call and write the `Layout` there. In `free`, we read that
//! information back to pass it to `dealloc`.
//!
//! For Rust's allocations, we are required to specify an alignment. We align to the size of
//! `usize` which should be a safe default for all architectures.
//!
//! Although mbedtls usage is the application's concern, we can not define those functions
//! in the application because the application would not run anymore in std environments where
//! Rust's internal memory management may be based on libc.

extern crate alloc;
use alloc::alloc::{alloc_zeroed, dealloc, handle_alloc_error, Layout};
use core::ffi::{c_size_t, c_void};
use core::mem::size_of;

#[no_mangle]
pub unsafe extern "C" fn calloc(nitems: c_size_t, size: c_size_t) -> *mut c_void {
    let layout =
        Layout::from_size_align(size_of::<Layout>() + size * nitems, size_of::<usize>()).unwrap();
    let ptr = alloc_zeroed(layout);
    if ptr.is_null() {
        handle_alloc_error(layout);
    } else {
        core::ptr::write(ptr as *mut Layout, layout);
        ptr.add(size_of::<Layout>()) as *mut c_void
    }
}

#[no_mangle]
pub unsafe extern "C" fn free(ptr: *mut c_void) {
    if !ptr.is_null() {
        let ptr = ptr.sub(size_of::<Layout>());
        let layout = core::ptr::read(ptr as *const Layout);
        dealloc(ptr as *mut u8, layout)
    }
}
