// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! Provides a `global_allocator` with 80 kB size.

use alloc_cortex_m::CortexMHeap;
use core::alloc;
use core::mem::MaybeUninit;

#[global_allocator]
static ALLOCATOR: CortexMHeap = CortexMHeap::empty();

pub fn init_allocator() {
    const HEAP_SIZE: usize = 80 * 1024;
    static mut HEAP: [MaybeUninit<u8>; HEAP_SIZE] = [MaybeUninit::uninit(); HEAP_SIZE];
    unsafe { ALLOCATOR.init(HEAP.as_ptr() as usize, HEAP_SIZE) }
}

#[alloc_error_handler]
fn oom(_: alloc::Layout) -> ! {
    panic!("OOM");
}
