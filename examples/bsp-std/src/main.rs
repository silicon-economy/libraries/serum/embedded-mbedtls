// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! This BSP implements the required hardware interface for `std` environments

use core::ffi;
use std::net::{Ipv4Addr, UdpSocket};
use std::time::Instant;

use std_embedded_nal::Stack;

use embedded_mbedtls_sys as mtls_sys;

#[cfg(not(any(feature = "dtls-psk-client", feature = "dtls-psk-client-sys")))]
compile_error!("Select one of the available examples by activating the corresponding feature, e.g. 'dtls-psk-client'");

fn main() {
    flexi_logger::Logger::try_with_env_or_str("info")
        .expect("Could not create logger")
        .start()
        .expect("Could not start logger");

    #[cfg(feature = "dtls-psk-client-sys")]
    run_dtls_psk_client_sys();

    #[cfg(feature = "dtls-psk-client")]
    run_dtls_psk_client();
}

unsafe extern "C" fn my_entropy_fn(
    _entropy_context: *mut ffi::c_void,
    buf: *mut ffi::c_uchar,
    buf_len: usize,
) -> ffi::c_int {
    let bytes = core::slice::from_raw_parts_mut(buf, buf_len);
    let mut rng = rand::thread_rng();
    use rand::RngCore;
    rng.fill_bytes(bytes);
    0
}

unsafe extern "C" fn my_send_fn(
    ctx: *mut ffi::c_void,
    buf: *const ffi::c_uchar,
    len: usize,
) -> ffi::c_int {
    let udp: &mut UdpSocket = &mut *(ctx as *mut UdpSocket);
    let data = core::slice::from_raw_parts(buf, len);
    udp.send(data).expect("Could not send UDP datagram");
    data.len() as ffi::c_int
}

unsafe extern "C" fn my_recv_fn(
    ctx: *mut ffi::c_void,
    buf: *mut ffi::c_uchar,
    len: usize,
) -> ffi::c_int {
    let udp: &mut UdpSocket = &mut *(ctx as *mut UdpSocket);
    let data = core::slice::from_raw_parts_mut(buf, len);
    loop {
        match udp.recv(data) {
            Ok(len) => {
                return len as ffi::c_int;
            }
            Err(e) if e.kind() == std::io::ErrorKind::WouldBlock => {
                return mtls_sys::MBEDTLS_ERR_SSL_WANT_READ;
            }
            Err(e) => {
                panic!("{e:?}");
            }
        }
    }
}

struct MyTimer {
    start: Instant,
    int_ms: u32,
    fin_ms: u32,
}

unsafe extern "C" fn my_set_timer_fn(timer: *mut ffi::c_void, int_ms: u32, fin_ms: u32) {
    let timer: &mut MyTimer = &mut *(timer as *mut MyTimer);
    timer.start = Instant::now();
    timer.int_ms = int_ms;
    timer.fin_ms = fin_ms;
}

unsafe extern "C" fn my_get_timer_fn(timer: *mut ffi::c_void) -> ffi::c_int {
    let timer: &mut MyTimer = &mut *(timer as *mut MyTimer);
    let duration = Instant::now().duration_since(timer.start).as_millis() as u32;
    if timer.fin_ms == 0 {
        -1
    } else if duration >= timer.fin_ms {
        2
    } else if duration >= timer.int_ms {
        1
    } else {
        0
    }
}

#[allow(unused)]
fn run_dtls_psk_client_sys() {
    let mut udp =
        UdpSocket::bind((Ipv4Addr::UNSPECIFIED, 0)).expect("Could not create/bind UDP socket");
    udp.connect("localhost:12345")
        .expect("Could not connect UDP socket");
    udp.set_nonblocking(true)
        .expect("Could not configure UDP socket as nonblocking");

    let mut timer = MyTimer {
        start: Instant::now(),
        int_ms: 0,
        fin_ms: 0,
    };

    unsafe {
        dtls_psk_client_sys::run(
            my_entropy_fn,
            core::ptr::null_mut(),
            &mut udp as *mut UdpSocket as *mut ffi::c_void,
            my_send_fn,
            my_recv_fn,
            &mut timer as *mut MyTimer as *mut ffi::c_void,
            my_set_timer_fn,
            my_get_timer_fn,
            exit,
        )
    };
}

struct StdClock;

impl embedded_timers::clock::Clock for StdClock {
    type Instant = std::time::Instant;
    fn now(&self) -> Self::Instant {
        std::time::Instant::now()
    }
}

#[allow(unused)]
fn run_dtls_psk_client() {
    // Set up architecture-dependent requirements (in this case: for std environments)
    let rng = rand::thread_rng();
    let udp_stack = Stack;
    let clock = StdClock;

    // Call the actual example code
    dtls_psk_client::run(udp_stack, clock, rng);
}

fn exit(ret: ffi::c_int) -> ! {
    unsafe {
        if ret != 0 {
            let mut error_buf: [core::ffi::c_char; 100] = [0; 100];
            mtls_sys::mbedtls_strerror(ret, &mut error_buf as *mut core::ffi::c_char, 100);
            let error_msg = core::ffi::CStr::from_ptr(&error_buf as *const core::ffi::c_char)
                .to_str()
                .expect("str should be utf-8");
            log::error!("Last error was: -0x{:x} - {}", -ret, error_msg);
        }
    }
    panic!("Exiting ...");
}
