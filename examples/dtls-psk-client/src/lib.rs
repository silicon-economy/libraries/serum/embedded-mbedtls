// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! This example opens a PSK-only DTLS connection as a client to a DTLS server using the high-level
//! Rust wrapper interface
//!
//! For example, the following command can be used to start a local `openssl s_server` test server:
//! `nix-shell --pure --run "openssl s_server -dtls -psk 01020304 -psk_identity embedded-mbedtls -nocert -port 12345 -cipher PSK-AES128-CCM -status_verbose -security_debug -security_debug_verbose -state -naccept 1"`
//!
//! For the openssl test server, the explicit cipher selection seems to be necessary. If it is
//! omitted, the server only offers an other set of ciphers and fails with a "no shared ciphers"
//! alert.

#![no_std]

extern crate alloc;

use alloc::string::String;
use core::net::{Ipv4Addr, SocketAddr, SocketAddrV4};
use embedded_mbedtls::ssl::{Preset, SslConnection, SslContext};
use embedded_nal::UdpClientStack;
use embedded_timers::clock::Clock;
use nb::block;
use rand_core::{CryptoRng, RngCore};

pub fn run(udp_stack: impl UdpClientStack, clock: impl Clock, rng: (impl CryptoRng + RngCore)) {
    let addr = SocketAddr::V4(SocketAddrV4::new(Ipv4Addr::new(127, 0, 0, 1), 12345));

    let mut context = SslContext::new_udp_client_side(udp_stack, &clock, rng, addr);

    log::info!("Create and configure DTLS client");

    let mut connection = SslConnection::new_dtls_client(&mut context, Preset::Default).unwrap();

    connection
        .configure_psk(&[1, 2, 3, 4], "embedded-mbedtls".as_bytes())
        .unwrap();

    log::info!("Set up connection (handshake)");
    block!(connection.handshake()).unwrap();

    log::info!("Send 'Hello, embedded-mbedtls' to server");
    block!(connection.write("Hello, embedded-mbedtls".as_bytes())).unwrap();

    log::info!("Read response from server");
    let mut buf = [0u8; 1024];
    let len = block!(connection.read(&mut buf)).unwrap();
    let rx = String::from_utf8_lossy(&buf[0..len]);
    log::info!("Received: {rx}");

    log::info!("Close connection");
    block!(connection.close_notify()).unwrap();
}
