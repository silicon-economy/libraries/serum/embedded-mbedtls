# This nix environment can be used to build embedded-mbedtls projects for embedded ARM targets
# (e.g. thumbv7em-none-eabihf) using ARM's GNU Arm Embedded Toolchain (gcc-arm-embedded).
#
# The parameter `thumbTarget` needs to be set to e.g. "thumbv7em-none-eabihf". For example:
# `nix-shell arm-embedded.nix --argstr thumbTarget thumbv7em-none-eabihf`
#
# The parameter `nightly` is set to true by default because currently, we still need nightly for
# no_std alloc.
{ nightly ? true, thumbTarget }:
let
  common = import ./common.nix;

  pkgs = common.nixpkgsBase {
    overlays = [ common.rust_overlay common.serum_overlay ];
  };

  # Choose between a specific stable Rust version and a nightly version identified by date
  rust = (if nightly
      then pkgs.rust-bin.nightly."2025-02-26"
      else pkgs.rust-bin.stable."1.82.0"
    ).default.override {
      targets = [ thumbTarget ];
    };

  # Without the following package, generating the bindings fails on Linux-x86_64 with:
  # "fatal error: 'gnu/stubs-32.h' file not found"
  optionalGlibc32 = if pkgs.stdenv.buildPlatform.isLinux && pkgs.stdenv.buildPlatform.isx86_64
    then [ pkgs.glibc_multi ]
    else [];
in
  
  pkgs.stdenv.mkDerivation {
    name = "rust-env";

    buildInputs = with pkgs; [
      # Rust and C toolchain
      rust
      gcc-arm-embedded # to cross-build Mbed TLS C library
      cmake # to build Mbed TLS C library
      clang # for bindgen
      # Additional tooling
      git
      openssh # to get gitlab dependencies from within the nix shell
      cacert # for certificate verification when downloading from crates.io
      # Nix debugging
      which
    ] ++ optionalGlibc32;

    # Set Environment Variables
    RUST_BACKTRACE = 1;

    # For C cross-compilation
    CC_thumbv6m_none_eabi = "arm-none-eabi-gcc";
    CC_thumbv7m_none_eabi = "arm-none-eabi-gcc";
    CC_thumbv7em_none_eabi = "arm-none-eabi-gcc";
    CC_thumbv7em_none_eabihf = "arm-none-eabi-gcc";
    LIBCLANG_PATH = "${pkgs.clang.cc.lib.outPath}/lib";
  }
