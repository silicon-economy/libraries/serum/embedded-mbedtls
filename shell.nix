# This nix environment can be used to build embedded-mbedtls projects in std environments.
#
# `nix-shell [shell.nix]` creates an environment for native compilation with a stable Rust toolchain.
#
# The parameter `nightly` can be set to true with `--arg nightly true` to get a nightly Rust
# toolchain.
#
# The parameter `crossConfig` can be set to a nix crossSystem config string for a cross compilation
# environment.
# For example, `nix-shell --argstr crossConfig "armv7l-unknown-linux-gnueabihf"` will
# create an environment which targets ARMv7, the projects/examples can then be built with
# `cargo build --target armv7-unknown-linux-gnueabihf` (note the extra/missing "l" there).
# Note however that other targets might require some additional tweaking here. For example, see the
# `CARGO_TARGET_ARMV7_UNKNOWN_LINUX_GNUEABIHF_LINKER` environment variable below which is required
# for this example target platform.
{ nightly ? false, crossConfig ? null }:
with import ./common.nix;
let
  overlays = [ rust_overlay serum_overlay ];
  nixpkgsConfig = if crossConfig != null
    then {
      crossSystem = {
        config = crossConfig;
      };
      overlays = overlays;
    }
    else {
      overlays = overlays;
    };
  pkgs = nixpkgsBase nixpkgsConfig;
in
pkgs.callPackage ({
  stdenv,
  buildPackages, libiconv, cmake, clang,
  gitlab-clippy, cargo2junit, cargo-binutils, cargo-udeps, cargo-tarpaulin, nodePackages, cargo-deny, cargo-readme,
  zlib, git, cacert, openssh, openssl,
  rust-bin,
  which
}:

  stdenv.mkDerivation {
    name = "rust-env";

    depsBuildBuild = [
        # Native toolchain
        buildPackages.stdenv.cc # See https://nixos.org/manual/nixpkgs/stable/#ssec-cross-cookbook
        libiconv # Required for cross-linking on macOS
        # CI/CD pipeline tools
        cargo-readme
        gitlab-clippy
        cargo2junit
        cargo-binutils
        cargo-udeps
        cargo-tarpaulin
        nodePackages.cspell
        cargo-deny
        # Required in gitlab-runner for git access and secure downloads
        zlib
        git
        cacert
        openssh
        openssl
        # buildInputs must also be available natively so that the tests
        # can be run on the build host
        # ...

        # just for debugging the nix setup itself in pure shells
        which
      ];

    nativeBuildInputs = [
      cmake
      clang
      (if nightly
          then rust-bin.nightly."2025-02-27"
          else rust-bin.stable."1.82.0"
      ).default
    ];

    buildInputs = [
    ];

    # The nix gcc-wrapper always enables -D_FORTIFY_SOURCE which requires optimizations -O, see
    # https://github.com/NixOS/nixpkgs/issues/18995
    # This prevents non-release builds of Mbed TLS when targeting Linux and gcc is used. Then, the
    # build fails with:
    # `error: #warning _FORTIFY_SOURCE requires compiling with optimization (-O) [-Werror=cpp]`
    # Therefore, we turn this off to also enable debug builds.
    hardeningDisable = [ "fortify" ];

    # Set Environment Variables
    RUST_BACKTRACE = 1;

    # Specify the linker to use for cross compilation. With this environment
    # variable set, we do not need to specify it in .cargo/config.toml anymore.
    CARGO_TARGET_ARMV7_UNKNOWN_LINUX_GNUEABIHF_LINKER = "${stdenv.cc.targetPrefix}gcc";

    # Nix sets the CC environment variable to
    # "armv7-unknown-linux-gnueabihf-gcc". When building the xtasks or building
    # build.rs, cargo/Rust sets its own compilation target to e.g.
    # "x86_64-unknown-linux-gnu" but uses the compiler specified in CC instead
    # of the native one. This can be overridden with a specific environment
    # variable just for that target.
    CC_x86_64_unknown_linux_gnu = "gcc";
    CXX_x86_64_unknown_linux_gnu = "g++";
    CC_aarch64_unknown_linux_gnu = "gcc";
    CXX_aarch64_unknown_linux_gnu = "g++";
    CC_aarch64_apple_darwin = "clang";
    CXX_aarch64_apple_darwin = "clang++";

    # bindgen needs to be pointed to the right clang binary and libclang.so
    # path. Otherwise, it might accidentally use a native clang which may miss
    # some headers or fail otherwise.
    LIBCLANG_PATH = "${pkgs.pkgsBuildHost.clang.cc.lib.outPath}/lib";
    CLANG_PATH = "${pkgs.pkgsBuildHost.clang.outPath}/bin/${stdenv.cc.targetPrefix}clang";
}) {}
