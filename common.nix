# Common stuff for all environments
rec
{
  # Oxalica's Rust overlay gives us the rust-bin function which allows us
  # to select a specific Rust toolchain. Furthermore, we can configure
  # additional targets like shown below.
  rust_overlay = import (builtins.fetchTarball {
    name = "rust-overlay-2024-08-14";
    url = "https://github.com/oxalica/rust-overlay/archive/d342e8b5fd88421ff982f383c853f0fc78a847ab.tar.gz";
    sha256 = "1hrbc8hg0vmg6bwd30kmyi1hy9g0vm0hs197jlfqlsv8nr14y9yx";
  });

  # Our own overlay with additional build environment tools
  serum_overlay = import (builtins.fetchGit {
    name = "serum-overlay-2023-05-05";
    url = "https://git.openlogisticsfoundation.org/silicon-economy/libraries/serum/serum-nix-overlay.git";
    allRefs = true;
    rev = "d3781d433a1ca275c3882470dfb9b7eaeec9c0d6";
  });

  # Pinned nixpkgs
  nixpkgsBase = import (builtins.fetchTarball {
    name = "nixpkgs-stable-24.05";
    url = "https://github.com/NixOS/nixpkgs/archive/refs/tags/24.05.tar.gz";
    # sha256 = "0000000000000000000000000000000000000000000000000000";
    sha256 = "1lr1h35prqkd1mkmzriwlpvxcb34kmhc9dnr48gkm8hh089hifmx";
  });
}
