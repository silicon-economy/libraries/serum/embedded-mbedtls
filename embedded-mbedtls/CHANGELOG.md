# Changelog

## Current Development

## v0.2.0
- Upgrade to `embedded-timers` 0.4.0
- Upgrade to `embedded-nal` 0.9.0

## v0.1.1
- Version bump for a new crates.io release with README.md fixes

## v0.1.0
 - DTLS (1.2) support
 - Client mode
 - PSK support
 - CTR_DRBG pseudo-random number generator support
