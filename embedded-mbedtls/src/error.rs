// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! Error type module which defines the [`MbedtlsError`] type which describes known Mbed TLS error
//! codes and the [`Error`] type for all errors in this library

use num_enum::TryFromPrimitive;

/// Error type for all errors (Mbed TLS and other layers)
///
/// Mbed TLS uses C `int`s for its error codes using negative values from the `i16` range. Whenever
/// an Mbed TLS error is forwarded, we try to translate it into an [`MbedtlsError`]. If this
/// succeeds, the error variant will be [`Error::Mbedtls`], if this fails the variant will be
/// [`Error::Unknown`].
#[derive(Clone, Copy, Eq, PartialEq, Debug)]
#[non_exhaustive]
pub enum Error {
    /// Known errors from Mbed TLS
    Mbedtls(MbedtlsError),
    /// Unknown errors from Mbed TLS
    Unknown(core::ffi::c_int),
}

impl From<core::ffi::c_int> for Error {
    fn from(value: core::ffi::c_int) -> Self {
        let Ok(code): Result<i16, core::num::TryFromIntError> = value.try_into() else {
            log::error!("Mbed TLS error code {value} is out of i16 range.");
            return Error::Unknown(value);
        };
        if let Ok(mbedtls_error) = code.try_into() {
            Error::Mbedtls(mbedtls_error)
        } else {
            log::warn!("Encountered unknown Mbed TLS error code {value}");
            Error::Unknown(value)
        }
    }
}

/// Known mbedtls error codes
//
// To re-create or check this list, all error codes can be found in the generated `bindings.rs`.
// Filter this by the MBEDTLS_ERR_ prefix and adjust with the scripting language or text editor
// features of your choice.
#[derive(Clone, Copy, TryFromPrimitive, Eq, PartialEq, Debug)]
#[non_exhaustive]
#[repr(i16)]
pub enum MbedtlsError {
    MpiFileIoError = embedded_mbedtls_sys::MBEDTLS_ERR_MPI_FILE_IO_ERROR as i16,
    MpiBadInputData = embedded_mbedtls_sys::MBEDTLS_ERR_MPI_BAD_INPUT_DATA as i16,
    MpiInvalidCharacter = embedded_mbedtls_sys::MBEDTLS_ERR_MPI_INVALID_CHARACTER as i16,
    MpiBufferTooSmall = embedded_mbedtls_sys::MBEDTLS_ERR_MPI_BUFFER_TOO_SMALL as i16,
    MpiNegativeValue = embedded_mbedtls_sys::MBEDTLS_ERR_MPI_NEGATIVE_VALUE as i16,
    MpiDivisionByZero = embedded_mbedtls_sys::MBEDTLS_ERR_MPI_DIVISION_BY_ZERO as i16,
    MpiNotAcceptable = embedded_mbedtls_sys::MBEDTLS_ERR_MPI_NOT_ACCEPTABLE as i16,
    MpiAllocFailed = embedded_mbedtls_sys::MBEDTLS_ERR_MPI_ALLOC_FAILED as i16,
    EcpBadInputData = embedded_mbedtls_sys::MBEDTLS_ERR_ECP_BAD_INPUT_DATA as i16,
    EcpBufferTooSmall = embedded_mbedtls_sys::MBEDTLS_ERR_ECP_BUFFER_TOO_SMALL as i16,
    EcpFeatureUnavailable = embedded_mbedtls_sys::MBEDTLS_ERR_ECP_FEATURE_UNAVAILABLE as i16,
    EcpVerifyFailed = embedded_mbedtls_sys::MBEDTLS_ERR_ECP_VERIFY_FAILED as i16,
    EcpAllocFailed = embedded_mbedtls_sys::MBEDTLS_ERR_ECP_ALLOC_FAILED as i16,
    EcpRandomFailed = embedded_mbedtls_sys::MBEDTLS_ERR_ECP_RANDOM_FAILED as i16,
    EcpInvalidKey = embedded_mbedtls_sys::MBEDTLS_ERR_ECP_INVALID_KEY as i16,
    EcpSigLenMismatch = embedded_mbedtls_sys::MBEDTLS_ERR_ECP_SIG_LEN_MISMATCH as i16,
    EcpInProgress = embedded_mbedtls_sys::MBEDTLS_ERR_ECP_IN_PROGRESS as i16,
    MdFeatureUnavailable = embedded_mbedtls_sys::MBEDTLS_ERR_MD_FEATURE_UNAVAILABLE as i16,
    MdBadInputData = embedded_mbedtls_sys::MBEDTLS_ERR_MD_BAD_INPUT_DATA as i16,
    MdAllocFailed = embedded_mbedtls_sys::MBEDTLS_ERR_MD_ALLOC_FAILED as i16,
    MdFileIoError = embedded_mbedtls_sys::MBEDTLS_ERR_MD_FILE_IO_ERROR as i16,
    PkAllocFailed = embedded_mbedtls_sys::MBEDTLS_ERR_PK_ALLOC_FAILED as i16,
    PkTypeMismatch = embedded_mbedtls_sys::MBEDTLS_ERR_PK_TYPE_MISMATCH as i16,
    PkBadInputData = embedded_mbedtls_sys::MBEDTLS_ERR_PK_BAD_INPUT_DATA as i16,
    PkFileIoError = embedded_mbedtls_sys::MBEDTLS_ERR_PK_FILE_IO_ERROR as i16,
    PkKeyInvalidVersion = embedded_mbedtls_sys::MBEDTLS_ERR_PK_KEY_INVALID_VERSION as i16,
    PkKeyInvalidFormat = embedded_mbedtls_sys::MBEDTLS_ERR_PK_KEY_INVALID_FORMAT as i16,
    PkUnknownPkAlg = embedded_mbedtls_sys::MBEDTLS_ERR_PK_UNKNOWN_PK_ALG as i16,
    PkPasswordRequired = embedded_mbedtls_sys::MBEDTLS_ERR_PK_PASSWORD_REQUIRED as i16,
    PkPasswordMismatch = embedded_mbedtls_sys::MBEDTLS_ERR_PK_PASSWORD_MISMATCH as i16,
    PkInvalidPubkey = embedded_mbedtls_sys::MBEDTLS_ERR_PK_INVALID_PUBKEY as i16,
    PkInvalidAlg = embedded_mbedtls_sys::MBEDTLS_ERR_PK_INVALID_ALG as i16,
    PkUnknownNamedCurve = embedded_mbedtls_sys::MBEDTLS_ERR_PK_UNKNOWN_NAMED_CURVE as i16,
    PkFeatureUnavailable = embedded_mbedtls_sys::MBEDTLS_ERR_PK_FEATURE_UNAVAILABLE as i16,
    PkSigLenMismatch = embedded_mbedtls_sys::MBEDTLS_ERR_PK_SIG_LEN_MISMATCH as i16,
    PkBufferTooSmall = embedded_mbedtls_sys::MBEDTLS_ERR_PK_BUFFER_TOO_SMALL as i16,
    CipherFeatureUnavailable = embedded_mbedtls_sys::MBEDTLS_ERR_CIPHER_FEATURE_UNAVAILABLE as i16,
    CipherBadInputData = embedded_mbedtls_sys::MBEDTLS_ERR_CIPHER_BAD_INPUT_DATA as i16,
    CipherAllocFailed = embedded_mbedtls_sys::MBEDTLS_ERR_CIPHER_ALLOC_FAILED as i16,
    CipherInvalidPadding = embedded_mbedtls_sys::MBEDTLS_ERR_CIPHER_INVALID_PADDING as i16,
    CipherFullBlockExpected = embedded_mbedtls_sys::MBEDTLS_ERR_CIPHER_FULL_BLOCK_EXPECTED as i16,
    CipherAuthFailed = embedded_mbedtls_sys::MBEDTLS_ERR_CIPHER_AUTH_FAILED as i16,
    CipherInvalidContext = embedded_mbedtls_sys::MBEDTLS_ERR_CIPHER_INVALID_CONTEXT as i16,
    Sha1BadInputData = embedded_mbedtls_sys::MBEDTLS_ERR_SHA1_BAD_INPUT_DATA as i16,
    Sha256BadInputData = embedded_mbedtls_sys::MBEDTLS_ERR_SHA256_BAD_INPUT_DATA as i16,
    Sha512BadInputData = embedded_mbedtls_sys::MBEDTLS_ERR_SHA512_BAD_INPUT_DATA as i16,
    Sha3BadInputData = embedded_mbedtls_sys::MBEDTLS_ERR_SHA3_BAD_INPUT_DATA as i16,
    Poly1305BadInputData = embedded_mbedtls_sys::MBEDTLS_ERR_POLY1305_BAD_INPUT_DATA as i16,
    ChachapolyBadState = embedded_mbedtls_sys::MBEDTLS_ERR_CHACHAPOLY_BAD_STATE as i16,
    ChachapolyAuthFailed = embedded_mbedtls_sys::MBEDTLS_ERR_CHACHAPOLY_AUTH_FAILED as i16,
    Chacha20BadInputData = embedded_mbedtls_sys::MBEDTLS_ERR_CHACHA20_BAD_INPUT_DATA as i16,
    SslCryptoInProgress = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_CRYPTO_IN_PROGRESS as i16,
    SslFeatureUnavailable = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_FEATURE_UNAVAILABLE as i16,
    SslBadInputData = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_BAD_INPUT_DATA as i16,
    SslInvalidMac = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_INVALID_MAC as i16,
    SslInvalidRecord = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_INVALID_RECORD as i16,
    SslConnEof = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_CONN_EOF as i16,
    SslDecodeError = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_DECODE_ERROR as i16,
    SslNoRng = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_NO_RNG as i16,
    SslNoClientCertificate = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_NO_CLIENT_CERTIFICATE as i16,
    SslUnsupportedExtension = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_UNSUPPORTED_EXTENSION as i16,
    SslNoApplicationProtocol = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_NO_APPLICATION_PROTOCOL as i16,
    SslPrivateKeyRequired = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_PRIVATE_KEY_REQUIRED as i16,
    SslCaChainRequired = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_CA_CHAIN_REQUIRED as i16,
    SslUnexpectedMessage = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_UNEXPECTED_MESSAGE as i16,
    SslFatalAlertMessage = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_FATAL_ALERT_MESSAGE as i16,
    SslUnrecognizedName = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_UNRECOGNIZED_NAME as i16,
    SslPeerCloseNotify = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_PEER_CLOSE_NOTIFY as i16,
    SslBadCertificate = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_BAD_CERTIFICATE as i16,
    SslReceivedNewSessionTicket =
        embedded_mbedtls_sys::MBEDTLS_ERR_SSL_RECEIVED_NEW_SESSION_TICKET as i16,
    SslCannotReadEarlyData = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_CANNOT_READ_EARLY_DATA as i16,
    SslReceivedEarlyData = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_RECEIVED_EARLY_DATA as i16,
    SslCannotWriteEarlyData = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_CANNOT_WRITE_EARLY_DATA as i16,
    SslCacheEntryNotFound = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_CACHE_ENTRY_NOT_FOUND as i16,
    SslAllocFailed = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_ALLOC_FAILED as i16,
    SslHwAccelFailed = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_HW_ACCEL_FAILED as i16,
    SslHwAccelFallthrough = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_HW_ACCEL_FALLTHROUGH as i16,
    SslBadProtocolVersion = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_BAD_PROTOCOL_VERSION as i16,
    SslHandshakeFailure = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_HANDSHAKE_FAILURE as i16,
    SslSessionTicketExpired = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_SESSION_TICKET_EXPIRED as i16,
    SslPkTypeMismatch = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_PK_TYPE_MISMATCH as i16,
    SslUnknownIdentity = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_UNKNOWN_IDENTITY as i16,
    SslInternalError = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_INTERNAL_ERROR as i16,
    SslCounterWrapping = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_COUNTER_WRAPPING as i16,
    SslWaitingServerHelloRenego =
        embedded_mbedtls_sys::MBEDTLS_ERR_SSL_WAITING_SERVER_HELLO_RENEGO as i16,
    SslHelloVerifyRequired = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_HELLO_VERIFY_REQUIRED as i16,
    SslBufferTooSmall = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_BUFFER_TOO_SMALL as i16,
    SslWantRead = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_WANT_READ as i16,
    SslWantWrite = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_WANT_WRITE as i16,
    SslTimeout = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_TIMEOUT as i16,
    SslClientReconnect = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_CLIENT_RECONNECT as i16,
    SslUnexpectedRecord = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_UNEXPECTED_RECORD as i16,
    SslNonFatal = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_NON_FATAL as i16,
    SslIllegalParameter = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_ILLEGAL_PARAMETER as i16,
    SslContinueProcessing = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_CONTINUE_PROCESSING as i16,
    SslAsyncInProgress = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_ASYNC_IN_PROGRESS as i16,
    SslEarlyMessage = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_EARLY_MESSAGE as i16,
    SslUnexpectedCid = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_UNEXPECTED_CID as i16,
    SslVersionMismatch = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_VERSION_MISMATCH as i16,
    SslBadConfig = embedded_mbedtls_sys::MBEDTLS_ERR_SSL_BAD_CONFIG as i16,
    AesInvalidKeyLength = embedded_mbedtls_sys::MBEDTLS_ERR_AES_INVALID_KEY_LENGTH as i16,
    AesInvalidInputLength = embedded_mbedtls_sys::MBEDTLS_ERR_AES_INVALID_INPUT_LENGTH as i16,
    AesBadInputData = embedded_mbedtls_sys::MBEDTLS_ERR_AES_BAD_INPUT_DATA as i16,
    EntropySourceFailed = embedded_mbedtls_sys::MBEDTLS_ERR_ENTROPY_SOURCE_FAILED as i16,
    EntropyMaxSources = embedded_mbedtls_sys::MBEDTLS_ERR_ENTROPY_MAX_SOURCES as i16,
    EntropyNoSourcesDefined = embedded_mbedtls_sys::MBEDTLS_ERR_ENTROPY_NO_SOURCES_DEFINED as i16,
    EntropyNoStrongSource = embedded_mbedtls_sys::MBEDTLS_ERR_ENTROPY_NO_STRONG_SOURCE as i16,
    EntropyFileIoError = embedded_mbedtls_sys::MBEDTLS_ERR_ENTROPY_FILE_IO_ERROR as i16,
    CtrDrbgEntropySourceFailed =
        embedded_mbedtls_sys::MBEDTLS_ERR_CTR_DRBG_ENTROPY_SOURCE_FAILED as i16,
    CtrDrbgRequestTooBig = embedded_mbedtls_sys::MBEDTLS_ERR_CTR_DRBG_REQUEST_TOO_BIG as i16,
    CtrDrbgInputTooBig = embedded_mbedtls_sys::MBEDTLS_ERR_CTR_DRBG_INPUT_TOO_BIG as i16,
    CtrDrbgFileIoError = embedded_mbedtls_sys::MBEDTLS_ERR_CTR_DRBG_FILE_IO_ERROR as i16,
    ErrorGenericError = embedded_mbedtls_sys::MBEDTLS_ERR_ERROR_GENERIC_ERROR as i16,
    ErrorCorruptionDetected = embedded_mbedtls_sys::MBEDTLS_ERR_ERROR_CORRUPTION_DETECTED as i16,
    PlatformHwAccelFailed = embedded_mbedtls_sys::MBEDTLS_ERR_PLATFORM_HW_ACCEL_FAILED as i16,
    PlatformFeatureUnsupported =
        embedded_mbedtls_sys::MBEDTLS_ERR_PLATFORM_FEATURE_UNSUPPORTED as i16,
}
