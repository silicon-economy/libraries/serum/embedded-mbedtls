// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! An [Mbed TLS](https://www.trustedfirmware.org/projects/mbed-tls/) Rust wrapper
//! for constrained embedded devices.
//!
//! Currently, this project is developed with `no_std` CoAPs and LwM2M devices in mind, though usage
//! is not limited to these use cases. Targeting CoAPs and LwM2M, it uses a static Mbed TLS configuration:
//! - DTLS support
//! - (D)TLS 1.2
//! - Client-only
//! - `TLS_PSK_WITH_AES_128_CCM_8` cipher suite support
//! - CTR_DRBG pseudo-random number generator support
//!
//! In the future, a dynamic configuration using Rust features might be implemented, but there is
//! currently no roadmap to do so.
//!
//! ## Platform abstraction
//! - Networking with [`embedded_nal`](https://docs.rs/embedded-nal/latest/embedded_nal/)
//! - Timing with [`embedded_timers`](https://docs.rs/embedded-timers/latest/embedded_timers/)
//! - Random Number Generation with [`rand_core`](https://docs.rs/rand_core/latest/rand_core/)
//!
//! # Usage Example
//! In addition to the following example code, also have a look at the example directory in the
//! git repository. Due to the strong focus on `no_std` environments, the repository contains
//! working examples for `no_std` hardware.
//! ```
//! # use core::net::{IpAddr, Ipv4Addr, SocketAddr};
//! # use embedded_nal::UdpClientStack;
//! # use embedded_timers::clock::Clock;
//! # use rand_core::{CryptoRng, RngCore};
//! use embedded_mbedtls::ssl::{SslConnection, SslContext, Preset};
//! use nb::block;
//!
//! # fn _setup_ssl_stack<U: UdpClientStack, R: RngCore + CryptoRng>(
//! #     net_stack: U,
//! #     clock: &impl Clock,
//! #     rng: R,
//! # ) {
//! #    let server_addr = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), 22);
//! // Assuming the variables in use contain a valid hardware abstraction and the server address
//! // Create the hardware context
//! let mut ctx = SslContext::new_udp_client_side(net_stack, clock, rng, server_addr);
//! // Create and configure the connection instance
//! let mut connection = SslConnection::new_dtls_client(&mut ctx, Preset::Default).unwrap();
//! connection
//!     .configure_psk(&[1, 2, 3, 4], "embedded-mbedtls".as_bytes())
//!     .unwrap();
//!
//! // Set up connection
//! block!(connection.handshake()).unwrap();
//!
//! // Send data
//! block!(connection.write("Hello, embedded-mbedtls".as_bytes())).unwrap();
//!
//! // Receive data
//! let mut buf = [0u8; 1024];
//! let len = block!(connection.read(&mut buf)).unwrap();
//!
//! // Close connection
//! block!(connection.close_notify()).unwrap();
//! # }
//! ```
//!
//! # Features
//! - `alloc`: enables use of heap allocated contexts, see the explanation in
//!   [`SslConnection`](ssl::SslConnection)
//!
//! # License
//!
//! Open Logistics License\
//! Version 1.3, January 2023
//!
//! See the LICENSE file in the top-level directory.
//!
//! # Contact
//!
//! Fraunhofer IML Embedded Rust Group - <embedded-rust@iml.fraunhofer.de>

#![cfg_attr(not(test), no_std)]

#[cfg(feature = "alloc")]
extern crate alloc;

pub mod error;
pub mod rng;
pub mod ssl;
mod timing;
pub mod udp;
