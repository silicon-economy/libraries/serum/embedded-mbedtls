# embedded-mbedtls

An [Mbed TLS](https://www.trustedfirmware.org/projects/mbed-tls/) sys-crate and Rust wrapper
for constrained embedded devices.

Currently, this project is developed with `no_std` CoAPs and LwM2M devices in mind, though usage
is not limited to these use cases. Targeting CoAPs and LwM2M, it uses a static Mbed TLS configuration:
- DTLS support
- (D)TLS 1.2
- Client-only
- `TLS_PSK_WITH_AES_128_CCM_8` cipher suite support

In the future, a dynamic configuration using Rust features might be implemented, but there is
currently no roadmap to do so.

Due to the strong focus on `no_std` environments, the repository contains
working examples for `no_std` hardware, see the examples section.

## Resources

- [Mbed TLS Website](https://www.trustedfirmware.org/projects/mbed-tls/)
- [Mbed TLS GitHub Repository](https://www.trustedfirmware.org/projects/mbed-tls/)
- [Mbed TLS Documentation](https://mbed-tls.readthedocs.io/en/latest/)

## Rust Wrapper and sys-crate
The project contains the `embedded-mbedtls-sys` sys-crate which builds the Mbed TLS C library
using CMake and generates Rust bindings using bindgen. The `embedded-mbedtls` Rust wrapper adds
a safe interface on top. The Rust wrapper uses the
[embedded-nal](https://crates.io/crates/embedded-nal),
[rand_core](https://crates.io/crates/rand_core) and
[embedded-timers](https://crates.io/crates/embedded-timers) interfaces/traits to abstract the
underlying UDP network stack, cryptographic random number generator and clock respectively.

## Mbed TLS version
The Mbed TLS library is managed as a git submodule under `embedded-mbedtls-sys/3rdparty/mbedtls`.
The version currently in use can be checked in
`embedded-mbedtls-sys/3rdparty/mbedtls/include/mbedtls/build_info.h` or by inspecting the submodule
repository.

## Examples
The `examples` directory contains examples using the Rust wrapper and examples using the sys-crate
directly. They can be run in an `std` environment as well as on `no_std` hardware. Currently, the
[STM32 Nucleo-L452RE](https://www.st.com/en/evaluation-tools/nucleo-l452re.html) board is supported
which should be easily available.

The packages in the example can roughly be grouped as follows:
- `dtls-psk-client` and `dtls-psk-client-sys` demonstrate the usage of the Rust wrapper and the
sys-crate respectively. They are implemented as Rust libraries in order to be used in/on different
environments or boards. They require function pointers or parameters
implementing specific traits for hardware access.
- `bsp-std` and `bsp-nucleo-l452re` implement the hardware access for `std` environments and the
STM32 Nucleo-L452RE board respectively. Using Rust features, the `dtls-psk-client` and
`dtls-psk-client-sys` examples can be run. The `bsp-nucleo-l452re` board support package requires
the `util-forward-serial-udp` utility, see below.
- `util-forward-serial-udp` must be run on the host machine when the examples are run on the
`bsp-nucleo-l452re` BSP. The `bsp-nucleo-l452re` examples transmit and receive UDP data via serial,
the `util-forward-serial-udp` translates these to actual UDP datagrams.

For further information, see the comments in the examples.

## Nix Files
The top-level `.nix` files declare reproducible build environments using the
[Nix package manager](https://nixos.org/). These are used for building/testing in the
CI/CD pipeline an can be used to run the examples. `common.nix` contains common definitions for all
environments, e.g. the pinned nixpkgs. The other `.nix` files are each used for a specific target
architecture (or group of target architectures):
- `shell.nix` can be used for `std` environments. Mainly, this is used for native builds, for example
in the CI/CD pipeline. However, when the `crossSystem` argument is set, this may also be used for
cross-compilation to other `std` targets, e.g. to embedded Linux systems.
- `arm-embedded.nix` can be used for embedded ARM targets
(e.g. thumbv7em-none-eabihf) using ARM's GNU Arm Embedded Toolchain (gcc-arm-embedded)

For further information, see the comments in the `.nix` files.

## License

Open Logistics License\
Version 1.3, January 2023

See the LICENSE file in the top-level directory.

## Contact

Fraunhofer IML Embedded Rust Group - <embedded-rust@iml.fraunhofer.de>
